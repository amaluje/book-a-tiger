package com.betomaluje.android.bookatigertest;

import android.app.Application;

import com.orm.SugarContext;

/**
 * Created by betomaluje on 6/13/16.
 */
public class BookApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        SugarContext.init(getApplicationContext());
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        SugarContext.terminate();
    }
}
