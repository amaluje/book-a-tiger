package com.betomaluje.android.bookatigertest.views.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.betomaluje.android.bookatigertest.R;
import com.betomaluje.android.bookatigertest.models.Job;
import com.betomaluje.android.bookatigertest.views.viewholders.JobsRecyclerViewHolder;

import java.util.List;

/**
 * Created by betomaluje on 6/13/16.
 */
public class JobsRecyclerAdapter extends RecyclerView.Adapter<JobsRecyclerViewHolder> {

    private List<Job> items;
    private LayoutInflater inflater;

    public JobsRecyclerAdapter(Context context, List<Job> items) {
        this.items = items;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public JobsRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.job_item_row, parent, false);
        return new JobsRecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(JobsRecyclerViewHolder holder, int position) {
        holder.fillData(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}

