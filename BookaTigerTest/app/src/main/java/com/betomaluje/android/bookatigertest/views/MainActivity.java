package com.betomaluje.android.bookatigertest.views;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.betomaluje.android.bookatigertest.R;
import com.betomaluje.android.bookatigertest.models.Job;
import com.betomaluje.android.bookatigertest.presenters.ClickedItem;
import com.betomaluje.android.bookatigertest.presenters.bus.BusStation;
import com.betomaluje.android.bookatigertest.presenters.jobs.IJobsView;
import com.betomaluje.android.bookatigertest.presenters.jobs.JobsPresenter;
import com.betomaluje.android.bookatigertest.views.adapters.JobsRecyclerAdapter;
import com.squareup.otto.Subscribe;

import java.util.List;

import io.github.sporklibrary.Spork;
import io.github.sporklibrary.annotations.BindComponent;
import io.github.sporklibrary.annotations.BindLayout;
import io.github.sporklibrary.annotations.BindView;

@BindLayout(R.layout.activity_main)
public class MainActivity extends AppCompatActivity implements IJobsView {

    @BindView(R.id.recyclerView_main)
    RecyclerView mRecyclerViewMain;

    @BindView(R.id.progressBar)
    ProgressBar mPogressBar;

    @BindView(R.id.textView_emptyView)
    TextView mTextViewEmpty;

    @BindComponent
    private JobsPresenter mJobsPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Spork.bind(this);

        mRecyclerViewMain.setHasFixedSize(true);
        mRecyclerViewMain.setLayoutManager(new LinearLayoutManager(MainActivity.this));
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusStation.getBus().register(this);
        mJobsPresenter.getJobs();
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusStation.getBus().unregister(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mJobsPresenter.destroy();
    }

    @Override
    public void onSuccessJobs(List<Job> jobs) {
        if (!jobs.isEmpty()) {
            JobsRecyclerAdapter adapter = new JobsRecyclerAdapter(MainActivity.this, jobs);
            mRecyclerViewMain.setAdapter(adapter);

            mRecyclerViewMain.setVisibility(View.VISIBLE);
            mTextViewEmpty.setVisibility(View.GONE);
        } else {
            mRecyclerViewMain.setVisibility(View.GONE);
            mTextViewEmpty.setVisibility(View.VISIBLE);
        }

        mPogressBar.setVisibility(View.GONE);
    }

    @Override
    public void onFailedJobs() {
        mPogressBar.setVisibility(View.GONE);
        mRecyclerViewMain.setVisibility(View.GONE);
        mTextViewEmpty.setVisibility(View.VISIBLE);

        if (!isFinishing()) {
            new AlertDialog.Builder(MainActivity.this)
                    .setTitle(R.string.getjobs_error_title)
                    .setMessage(R.string.getjobs_error_message)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            mJobsPresenter.getJobs();
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }
    }

    @Override
    public void onStartJobs() {
        mPogressBar.setVisibility(View.VISIBLE);
        mRecyclerViewMain.setVisibility(View.GONE);
        mTextViewEmpty.setVisibility(View.GONE);
    }

    @Subscribe
    public void onJobReceived(ClickedItem clickedItem) {
        if (clickedItem.getObject() instanceof Job) {
            Job job = (Job) clickedItem.getObject();

            JobDetailActivity.launchActivity(MainActivity.this, job);
        }
    }

}
