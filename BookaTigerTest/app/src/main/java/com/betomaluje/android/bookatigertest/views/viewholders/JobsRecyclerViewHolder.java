package com.betomaluje.android.bookatigertest.views.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.betomaluje.android.bookatigertest.R;
import com.betomaluje.android.bookatigertest.models.Job;
import com.betomaluje.android.bookatigertest.presenters.ClickedItem;
import com.betomaluje.android.bookatigertest.presenters.DateUtils;
import com.betomaluje.android.bookatigertest.presenters.bus.BusStation;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import io.github.sporklibrary.Spork;
import io.github.sporklibrary.annotations.BindView;

/**
 * Created by betomaluje on 6/13/16.
 */
public class JobsRecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.textView_client)
    TextView mTextViewClient;
    @BindView(R.id.textView_date)
    TextView mTextViewDate;
    @BindView(R.id.textView_price)
    TextView mTextViewPrice;
    @BindView(R.id.textView_distance)
    TextView mTextViewDistance;
    @BindView(R.id.textView_street)
    TextView mTextViewStreet;

    private DateTimeFormatter timeFormat = DateTimeFormat.forPattern("dd.MM.YYYY");

    private Job selectedJob;

    public JobsRecyclerViewHolder(View itemView) {
        super(itemView);
        Spork.bind(this);

        itemView.setOnClickListener(this);
    }

    public void fillData(Job job) {
        this.selectedJob = job;

        mTextViewClient.setText(job.getCustomerName());
        mTextViewDate.setText(timeFormat.print(DateUtils.convertDate(job.getJobDate())));
        mTextViewPrice.setText("$" + job.getPrice());

        if (!job.getDistance().isEmpty()) {
            mTextViewDistance.setVisibility(View.VISIBLE);
            mTextViewDistance.setText(String.format("%.2f", Float.valueOf(job.getDistance())) + " Km");
        } else {
            mTextViewDistance.setVisibility(View.INVISIBLE);
        }

        mTextViewStreet.setText(job.getJobStreet());
    }

    @Override
    public void onClick(View v) {
        if (selectedJob != null) {
            BusStation.postOnMain(new ClickedItem(selectedJob, v, getLayoutPosition()));
        }
    }
}
