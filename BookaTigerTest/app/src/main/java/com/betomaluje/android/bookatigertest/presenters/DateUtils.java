package com.betomaluje.android.bookatigertest.presenters;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;

/**
 * Created by betomaluje on 6/13/16.
 */
public class DateUtils {

    public static DateTime convertDate(String fecha) {
        return DateTime.parse(fecha, DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")).toDateTime(DateTimeZone.getDefault());
    }

}
