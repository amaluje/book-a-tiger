package com.betomaluje.android.bookatigertest.presenters.jobs;

import com.betomaluje.android.bookatigertest.models.Job;

import java.util.List;

/**
 * Created by betomaluje on 6/13/16.
 */
public interface IJobsView {

    void onSuccessJobs(List<Job> jobs);

    void onFailedJobs();

    void onStartJobs();

}
