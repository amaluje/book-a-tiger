package com.betomaluje.android.bookatigertest.presenters;

import android.view.View;

/**
 * Created by betomaluje on 3/15/16.
 */
public class ClickedItem {

    Object object;
    View view;
    int position;

    public ClickedItem(Object object, View view, int position) {
        this.object = object;
        this.view = view;
        this.position = position;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public View getView() {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
