package com.betomaluje.android.bookatigertest.presenters.retrofit;

import com.betomaluje.android.bookatigertest.models.Job;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

/**
 * Created by betomaluje on 6/13/16.
 */
public class JobsManager {

    public static Call<ArrayList<Job>> getJobs(Retrofit retrofit, Callback<ArrayList<Job>> cb) {
        // prepare call in Retrofit 2.0
        IJobs jobsAPI = retrofit.create(IJobs.class);

        Call<ArrayList<Job>> call = jobsAPI.getJobs();
        //asynchronous call
        call.enqueue(cb);

        return call;
    }

}
