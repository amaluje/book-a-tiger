package com.betomaluje.android.bookatigertest.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;
import com.orm.dsl.Unique;

/**
 * Created by betomaluje on 6/13/16.
 */
public class Job extends SugarRecord implements Parcelable {

    public enum Recurrence {
        once("ONCE"),
        weekly("WEEKLY"),
        twoWeeks("Every 2 weeks"),
        monthly("MONTHLY");

        private final String name;

        Recurrence(String name) {
            this.name = name;
        }

        public boolean equalsName(String otherName) {
            return (otherName == null) ? false : name.equals(otherName);
        }

        public String toString() {
            return this.name;
        }
    }

    @SerializedName("customer_name")
    @Expose
    private String customerName;
    @SerializedName("distance")
    @Expose
    private String distance;
    @SerializedName("job_date")
    @Expose
    private String jobDate;
    @SerializedName("extras")
    @Expose
    private String extras;
    @SerializedName("order_duration")
    @Expose
    private String orderDuration;
    @SerializedName("order_id")
    @Expose
    @Unique
    private String orderId;
    @SerializedName("order_time")
    @Expose
    private String orderTime;
    @SerializedName("payment_method")
    @Expose
    private String paymentMethod;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("recurrency")
    @Expose
    private int recurrency;
    @SerializedName("job_city")
    @Expose
    private String jobCity;
    @SerializedName("job_latitude")
    @Expose
    private String jobLatitude;
    @SerializedName("job_longitude")
    @Expose
    private String jobLongitude;
    @SerializedName("job_postalcode")
    @Expose
    private int jobPostalcode;
    @SerializedName("job_street")
    @Expose
    private String jobStreet;
    @SerializedName("status")
    @Expose
    private String status;

    public Job() {
    }

    /**
     * @return The customerName
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * @return The distance
     */
    public String getDistance() {
        return distance;
    }

    /**
     * @return The jobDate
     */
    public String getJobDate() {
        return jobDate;
    }

    /**
     * @return The extras
     */
    public String getExtras() {
        return extras;
    }

    /**
     * @return The orderDuration
     */
    public String getOrderDuration() {
        return orderDuration;
    }

    /**
     * @return The orderId
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * @return The orderTime
     */
    public String getOrderTime() {
        return orderTime;
    }

    /**
     * @return The paymentMethod
     */
    public String getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * @return The price
     */
    public String getPrice() {
        return price;
    }

    /**
     * @return The recurrency
     */
    public String getRecurrency() {
        switch (recurrency) {
            case 0:
                return Recurrence.once.toString();
            case 7:
                return Recurrence.weekly.toString();
            case 14:
                return Recurrence.twoWeeks.toString();
            case 28:
                return Recurrence.monthly.toString();
            default:
                return Recurrence.once.toString();
        }
    }

    /**
     * @return The jobCity
     */
    public String getJobCity() {
        return jobCity;
    }

    /**
     * @return The jobLatitude
     */
    public String getJobLatitude() {
        return jobLatitude;
    }

    /**
     * @return The jobLongitude
     */
    public String getJobLongitude() {
        return jobLongitude;
    }

    /**
     * @return The jobPostalcode
     */
    public int getJobPostalcode() {
        return jobPostalcode;
    }

    /**
     * @return The jobStreet
     */
    public String getJobStreet() {
        return jobStreet;
    }

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    protected Job(Parcel in) {
        customerName = in.readString();
        distance = in.readString();
        jobDate = in.readString();
        extras = in.readString();
        orderDuration = in.readString();
        orderId = in.readString();
        orderTime = in.readString();
        paymentMethod = in.readString();
        price = in.readString();
        recurrency = in.readInt();
        jobCity = in.readString();
        jobLatitude = in.readString();
        jobLongitude = in.readString();
        jobPostalcode = in.readInt();
        jobStreet = in.readString();
        status = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(customerName);
        dest.writeString(distance);
        dest.writeString(jobDate);
        dest.writeString(extras);
        dest.writeString(orderDuration);
        dest.writeString(orderId);
        dest.writeString(orderTime);
        dest.writeString(paymentMethod);
        dest.writeString(price);
        dest.writeInt(recurrency);
        dest.writeString(jobCity);
        dest.writeString(jobLatitude);
        dest.writeString(jobLongitude);
        dest.writeInt(jobPostalcode);
        dest.writeString(jobStreet);
        dest.writeString(status);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Job> CREATOR = new Parcelable.Creator<Job>() {
        @Override
        public Job createFromParcel(Parcel in) {
            return new Job(in);
        }

        @Override
        public Job[] newArray(int size) {
            return new Job[size];
        }
    };
}
