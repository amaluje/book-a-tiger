package com.betomaluje.android.bookatigertest.presenters.retrofit;

import com.betomaluje.android.bookatigertest.models.Job;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by betomaluje on 6/13/16.
 */
public interface IJobs {

    @GET("jobs")
    Call<ArrayList<Job>> getJobs();

}
