package com.betomaluje.android.bookatigertest.presenters.jobs;

import com.betomaluje.android.bookatigertest.models.Job;
import com.betomaluje.android.bookatigertest.presenters.retrofit.RetrofitServiceManager;

import java.util.ArrayList;
import java.util.List;

import io.github.sporklibrary.Spork;
import io.github.sporklibrary.annotations.BindComponent;
import io.github.sporklibrary.annotations.ComponentParent;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by betomaluje on 6/13/16.
 */
public class JobsPresenter {

    @BindComponent
    private RetrofitServiceManager retrofitServiceManager;

    private IJobsView mJobsView;
    private Call currentCall;

    public JobsPresenter(@ComponentParent IJobsView jobsView) {
        Spork.bind(this);
        this.mJobsView = jobsView;
    }

    public void getJobs() {
        mJobsView.onStartJobs();

        currentCall = retrofitServiceManager.getJobs(new Callback<ArrayList<Job>>() {
            @Override
            public void onResponse(Call<ArrayList<Job>> call, Response<ArrayList<Job>> response) {
                if (response != null && response.body() != null) {
                    ArrayList<Job> data = response.body();
                    if (data != null && !data.isEmpty()) {

                        for (Job job : data) {

                            List<Job> jobs = Job.find(Job.class, "order_id = ?", job.getOrderId());

                            if (jobs.isEmpty()) {
                                job.save();
                            }

                        }

                        mJobsView.onSuccessJobs(data);
                    } else {
                        mJobsView.onFailedJobs();
                    }
                } else {

                    //we try to restore from internal database
                    List<Job> data = Job.listAll(Job.class);

                    if (data != null && !data.isEmpty()) {
                        mJobsView.onSuccessJobs(data);
                    } else {
                        mJobsView.onFailedJobs();
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Job>> call, Throwable t) {
                List<Job> data = Job.listAll(Job.class);

                if (data != null && !data.isEmpty()) {
                    mJobsView.onSuccessJobs(data);
                } else {
                    mJobsView.onFailedJobs();
                }
            }
        });
    }

    public void destroy() {
        if (currentCall != null)
            currentCall.cancel();
    }
}
