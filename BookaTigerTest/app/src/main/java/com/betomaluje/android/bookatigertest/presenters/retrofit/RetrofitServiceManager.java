package com.betomaluje.android.bookatigertest.presenters.retrofit;

import com.betomaluje.android.bookatigertest.models.Job;

import java.util.ArrayList;

import io.github.sporklibrary.annotations.ComponentScope;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by betomaluje on 6/13/16.
 */
@ComponentScope(ComponentScope.Scope.SINGLETON)
public class RetrofitServiceManager {

    public static final String BASE_URL = "http://private-14c693-rentapanda.apiary-mock.com/";

    private Retrofit retrofit;

    public RetrofitServiceManager() {

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

    }

    /*
   ===============  JOBS ===============
    */
    public Call<ArrayList<Job>> getJobs(Callback<ArrayList<Job>> cb) {
        return JobsManager.getJobs(retrofit, cb);
    }
}
