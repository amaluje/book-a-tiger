package com.betomaluje.android.bookatigertest.views;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.betomaluje.android.bookatigertest.R;
import com.betomaluje.android.bookatigertest.models.Job;
import com.betomaluje.android.bookatigertest.presenters.DateUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import io.github.sporklibrary.Spork;
import io.github.sporklibrary.annotations.BindLayout;
import io.github.sporklibrary.annotations.BindView;

/**
 * Created by betomaluje on 6/13/16.
 */
@BindLayout(R.layout.activity_job_detail)
public class JobDetailActivity extends AppCompatActivity {

    public static void launchActivity(Activity previousActivity, Job job) {
        Intent intent = new Intent(previousActivity, JobDetailActivity.class);

        Bundle b = new Bundle();
        b.putParcelable("job", job);
        intent.putExtras(b);

        previousActivity.startActivity(intent);
    }

    @BindView(R.id.textView_status)
    TextView mTextViewStatus;
    @BindView(R.id.textView_client)
    TextView mTextViewClient;
    @BindView(R.id.textView_date)
    TextView mTextViewDate;
    @BindView(R.id.textView_price)
    TextView mTextViewPrice;
    @BindView(R.id.textView_distance)
    TextView mTextViewDistance;
    @BindView(R.id.textView_street)
    TextView mTextViewStreet;
    @BindView(R.id.textView_recurrence)
    TextView mTextViewRecurrence;

    @BindView(R.id.linearLayout_details)
    View mLinearLayoutDetails;

    @BindView(R.id.progressBar)
    ProgressBar mPogressBar;

    @BindView(R.id.imageView_map)
    ImageView mImageViewMap;

    private DateTimeFormatter timeFormat = DateTimeFormat.forPattern("dd.MM.YYYY");

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Spork.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        getJobDetails((Job) getIntent().getExtras().getParcelable("job"));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

    private void getJobDetails(final Job job) {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        String latJob = job.getJobLatitude();
        String lonJob = job.getJobLongitude();
        String url = "http://maps.google.com/maps/api/staticmap?center=" + latJob + "," + lonJob + "&scale=2&zoom=15&size=" + width + "x" + height;

        Glide.with(JobDetailActivity.this)
                .load(url)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        mPogressBar.setVisibility(View.GONE);
                        mLinearLayoutDetails.setVisibility(View.VISIBLE);

                        mTextViewStatus.setText(getString(R.string.status_text) + " " + job.getStatus());

                        mTextViewClient.setText(job.getCustomerName());
                        mTextViewDate.setText(timeFormat.print(DateUtils.convertDate(job.getJobDate())));
                        mTextViewPrice.setText("$" + job.getPrice());

                        if (!job.getDistance().isEmpty()) {
                            mTextViewDistance.setVisibility(View.VISIBLE);
                            mTextViewDistance.setText(String.format("%.2f", Float.valueOf(job.getDistance())) + " Km");
                        } else {
                            mTextViewDistance.setVisibility(View.INVISIBLE);
                        }

                        mTextViewStreet.setText(job.getJobStreet());

                        mTextViewRecurrence.setText(getString(R.string.recurrence_text) + " " +
                                job.getRecurrency());

                        return false;
                    }
                })
                .into(mImageViewMap);
    }

}
